def external_validate(date, input_date_format, output_date_format):
    print("Validador en Layer")

    if date and not input_date_format:
        raise Exception("No se puede especificar una fecha sin un formato de entrada")
    elif input_date_format and not date:
        raise Exception("No se puede especificar un formato de entrada sin una fecha")
    elif not output_date_format:
        raise Exception("Se debe especificar un formato de salida")