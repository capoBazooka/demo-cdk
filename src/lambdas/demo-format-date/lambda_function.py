from datetime import datetime
from utils import external_validate

DATE_FORMAT = "%Y-%m-%d"

def validate(date, input_date_format, output_date_format):

    print("Validando")

    if date and not input_date_format:
        raise Exception("No se puede especificar una fecha sin un formato de entrada")
    elif input_date_format and not date:
        raise Exception("No se puede especificar un formato de entrada sin una fecha")
    elif not output_date_format:
        raise Exception("Se debe especificar un formato de salida")
    

def lambda_handler(event, context):

    date = event.get("Date")
    input_date_format = event.get("InputDateFormat")
    output_date_format = event.get("OutputDateFormat")

    #validate(date, input_date_format, output_date_format)
    external_validate(date, input_date_format, output_date_format)

    if date:
        date = datetime.strptime(date, input_date_format)
    else:
        date = datetime.now()

    return {
        "ParsedDate": date.strftime(output_date_format)
    }
