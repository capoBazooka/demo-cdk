import * as cdk from '@aws-cdk/core';
import { CfnParameter, Tag, Tags } from '@aws-cdk/core';
import { print } from 'util';
import { EventRuleDemo } from './event-rules';
import { LambdaFormatDate } from './lambdas'
import { UtilsLayer } from './layers';
import { StateDemoStepFunctions } from './state-machines';

export class ScheduleStatesStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // The code that defines your stack goes here

    const stackTags = this.node.tryGetContext("stackTags");
    const tagStack = stackTags === "y";
  

    const layerUtils = UtilsLayer(this);

    const lambdaFormatDate = LambdaFormatDate(this, [layerUtils]);

    const demoStateMachine = StateDemoStepFunctions(this, {
        LambdaFecha: lambdaFormatDate
    });

    EventRuleDemo(this, {
        StateMachine: demoStateMachine
    });

    if (tagStack) {
        Tags.of(this).add("Company", "Arkhotech");
    }
    
  }
}
