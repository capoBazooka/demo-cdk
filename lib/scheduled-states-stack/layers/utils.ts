import * as cdk from '@aws-cdk/core';
import * as lambda from '@aws-cdk/aws-lambda';

export const UtilsLayer = (scope: cdk.Construct) => {

  return new lambda.LayerVersion(scope, "UtilsLayer", {
    compatibleRuntimes: [lambda.Runtime.PYTHON_3_8],
    code: lambda.Code.fromAsset("./src/layers/utilities/"),
    layerVersionName: "UtilsLayer"
  });
} 