import { Function } from "@aws-cdk/aws-lambda";
import { Parallel, Pass, StateMachine, TaskInput } from "@aws-cdk/aws-stepfunctions";
import { LambdaInvoke } from "@aws-cdk/aws-stepfunctions-tasks";
import * as cdk from "@aws-cdk/core";
import { Duration } from "@aws-cdk/core";

interface StepProps {
    LambdaFecha: Function
};

export const StateDemoStepFunctions = (scope: cdk.Construct, props: StepProps) => {
    
    const taskParseFecha = new LambdaInvoke(scope, "Parse Fecha", {
        lambdaFunction: props.LambdaFecha,
        payload: TaskInput.fromObject({
            "OutputDateFormat": "$.OutputFormat"
        }),
        payloadResponseOnly: true,
        resultPath: "$.ParsedDate"
    });

    const taskPass1_1 = new Pass(scope, "Pass_1_1");
    const taskPass2_1 = new Pass(scope, "Pass_2_1");
    const taskPass2_2 = new Pass(scope, "Pass_2_2");
    
    const taskParallel = new Parallel(scope, "Parallelize", {});

    // Enlazar pasos    
    taskParseFecha
    .next(taskParallel
        .branch(
            taskPass1_1
        )
        .branch(
            taskPass2_1
            .next(taskPass2_2)
        )
    );

    const stateMachine = new StateMachine(scope, "DemoState", {
        definition: taskParseFecha,
        stateMachineName: "DemoStateMachine",
        timeout: Duration.minutes(5)
    });

    return stateMachine;
}