import { Schedule, Rule, RuleTargetInput } from '@aws-cdk/aws-events';
import { SfnStateMachine } from '@aws-cdk/aws-events-targets';
import { StateMachine } from '@aws-cdk/aws-stepfunctions';
import { Construct } from '@aws-cdk/core';

interface EventRuleDemo {
    StateMachine: StateMachine
}

export const EventRuleDemo = (scope: Construct, props: EventRuleDemo) => {
    const ruleInput = RuleTargetInput.fromObject({
        OutputFormat: "%Y/%m/%d"
    });

    new Rule(scope, "DemoEventRule", {
        description: "Ejecución periodica de la State especificada",
        enabled: false,
        schedule: Schedule.cron({
            minute: "0"
        }),
        targets: [new SfnStateMachine(props.StateMachine, {
            input: ruleInput
        })]
    });
}