import * as cdk from '@aws-cdk/core';
import { Function, LayerVersion, Runtime, Code } from '@aws-cdk/aws-lambda';
import { PolicyStatement } from '@aws-cdk/aws-iam';
import { CfnParameter, Tags } from '@aws-cdk/core';

export const LambdaFormatDate = (scope: cdk.Construct, layers: LayerVersion[]) => {

    const author = new CfnParameter(scope, "author", {
        type: "String",
        description: "Autor de los recursos del stack"
    });

    const ssmPolicy = new PolicyStatement();
    ssmPolicy.addActions("ssm:GetParameter");
    ssmPolicy.addAllResources();

    const lambda = new Function(scope, "DemoFormatDate", {
        functionName: "DemoFormatDate",
        runtime: Runtime.PYTHON_3_8,
        handler: "lambda_function.lambda_handler",
        code: Code.fromAsset("./src/lambdas/demo-format-date/"),
        layers: layers
    });

    lambda.addToRolePolicy(ssmPolicy);

    Tags.of(lambda).add("ResourceType", "Lambda");
    Tags.of(lambda).add("Author", author.valueAsString);

    return lambda;
}