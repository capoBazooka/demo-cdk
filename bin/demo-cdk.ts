#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import { DemoCdkStack } from '../lib/demo-cdk-stack';
import { ScheduleStatesStack } from '../lib/scheduled-states-stack/schedule-states-stack';

const app = new cdk.App();
new DemoCdkStack(app, 'DemoCdkStack', {});
new ScheduleStatesStack(app, "ScheduledStates")
